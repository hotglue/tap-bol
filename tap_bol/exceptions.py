class ServiceUnavailableError(Exception):
    """Raised when service is unavailable"""

    pass
