"""Stream type classes for tap-bol."""

from pathlib import Path

from singer_sdk import typing as th

from tap_bol.client import BolStream
from pendulum import parse
from typing import Iterable, Optional
from typing import Any, Optional, Dict
import requests

class OrdersStream(BolStream):
    """Define custom stream."""

    name = "orders"
    path = "/orders"
    primary_keys = ["orderId"]
    replication_key = None
    records_jsonpath = "$.orders[*]"
    already_parsed_order_ids = []

    schema = th.PropertiesList(
        th.Property("orderId", th.StringType),
        th.Property("orderPlacedDateTime", th.DateTimeType),
        th.Property(
            "orderItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderItemId", th.StringType),
                    th.Property("ean", th.StringType),
                    th.Property("fulfilmentMethod", th.StringType),
                    th.Property("fulfilmentStatus", th.StringType),
                    th.Property("quantity", th.NumberType),
                    th.Property("unitPrice", th.NumberType),
                    th.Property("quantityShipped", th.NumberType),
                    th.Property("quantityCancelled", th.NumberType),
                    th.Property("cancellationRequest", th.BooleanType),
                    th.Property("latestChangedDateTime", th.DateTimeType),
                )
            ),
        ),
    ).to_dict()

    def get_url_params(self, context, next_page_token):
        params = super().get_url_params(context, next_page_token)
        params["fulfilment-method"] = "ALL"
        params["status"] = "ALL"
        return params

    def parse_response(self, response):
        if response.json() == {}:
            return None

        for order in response.json()["orders"]:
            if order["orderId"] not in self.already_parsed_order_ids:
                self.already_parsed_order_ids.append(order["orderId"])
                yield order

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"order_id": record["orderId"]}

class ShipmentsStream(BolStream):
    """Define custom stream."""

    name = "shipments"
    path = "/shipments"
    primary_keys = ["shipmentId"]
    replication_key = None
    records_jsonpath = "$.shipments[*]"
    fulfillment_method = "FBR"

    schema = th.PropertiesList(
        th.Property("shipmentId", th.StringType),
        th.Property("shipmentDateTime", th.DateTimeType),
        th.Property("shipmentReference", th.StringType),
        th.Property(
            "order",
            th.ObjectType(
                th.Property("orderId", th.StringType),
                th.Property("orderPlacedDateTime", th.DateTimeType),
            ),
        ),
        th.Property(
            "shipmentItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderItemId", th.StringType),
                    th.Property("ean", th.StringType),
                    th.Property("unitPrice", th.NumberType),
                )
            ),
        ),
        th.Property(
            "transport",
            th.ObjectType(
                th.Property("transport", th.StringType),
            ),
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"shipment_id": record["shipmentId"]}
    
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if response.json() == {}:
            if self.fulfillment_method != "FBR":
                return None
            else:
                # use as dummie token to start requests with fulfillment_method FBB
                self.fulfillment_method = "FBB"
                return 1

        previous_token = previous_token or 1
        return previous_token + 1
    
    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        params["fulfilment-method"] = self.fulfillment_method
        return params

class OrderDetailsStream(BolStream):
    name = "order_details"
    path = "/orders/{order_id}"
    primary_keys = ["orderId"]
    replication_key = None
    records_jsonpath = "$.[*]"
    parent_stream_type = OrdersStream

    schema = th.PropertiesList(
        th.Property("orderId", th.StringType),
        th.Property("pickupPoint", th.BooleanType),
        th.Property("orderPlacedDateTime", th.DateTimeType),
        th.Property("shipmentDetails", th.CustomType({"type": ["object", "string"]})),
        th.Property("billingDetails", th.CustomType({"type": ["object", "string"]})),
        th.Property("orderItems", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

    def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
        ) -> Optional[Any]:
            """Return a token for identifying next page or None if no more pages."""

            return None
class ShipmentDetailsStream(BolStream):
    name = "shipment_details"
    path = "/shipments/{shipment_id}"
    primary_keys = ["orderId"]
    replication_key = None
    records_jsonpath = "$.[*]"
    parent_stream_type = ShipmentsStream

    schema = th.PropertiesList(
        th.Property("shipmentId", th.StringType),
        th.Property("shipmentDateTime", th.DateTimeType),
        th.Property("shipmentReference", th.StringType),
        th.Property("pickupPoint", th.BooleanType),
        th.Property("order", th.CustomType({"type": ["object", "string"]})),
        th.Property("shipmentDetails", th.CustomType({"type": ["object", "string"]})),
        th.Property("billingDetails", th.CustomType({"type": ["object", "string"]})),
        th.Property("shipmentItems", th.CustomType({"type": ["array", "string"]})),
        th.Property("transport", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

    def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
        ) -> Optional[Any]:
            """Return a token for identifying next page or None if no more pages."""

            return None
