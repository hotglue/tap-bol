"""Bol Authentication."""


import json
from typing import Optional

import requests
from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta
from singer_sdk.helpers._util import utc_now
from singer_sdk.streams import Stream as RESTStreamBase

import backoff
from http.client import RemoteDisconnected
from requests.exceptions import ConnectionError
from tap_bol.exceptions import ServiceUnavailableError


# The SingletonMeta metaclass makes your streams reuse the same authenticator instance.
# If this behaviour interferes with your use-case, you can remove the metaclass.
class BolAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for Bol."""

    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the Bol API."""
        # TODO: Define the request body needed for the API.
        return {
            # 'resource': 'https://analysis.windows.net/powerbi/api',
            # 'scope': self.oauth_scopes,
            "client_id": self.config["client_id"],
            "client_secret": self.config["client_secret"],
            "refresh_token": self.config["refresh_token"],
            "grant_type": "refresh_token",
        }

    @classmethod
    def create_for_stream(cls, stream) -> "BolAuthenticator":
        return cls(
            stream=stream,
            auth_endpoint="https://login.bol.com/token",
            # oauth_scopes="TODO: OAuth Scopes",
        )

    @backoff.on_exception(
        backoff.expo,
        (RemoteDisconnected, ConnectionError, ServiceUnavailableError),
        max_tries=5,
        factor=3,
    )
    def update_access_token(self) -> None:
        """Update `access_token` along with: `last_refreshed` and `expires_in`.

        Raises:
            RuntimeError: When OAuth login fails.
        """
        request_time = utc_now()
        auth_request_payload = self.oauth_request_payload
        token_response = requests.post(self.auth_endpoint, data=auth_request_payload)
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            if token_response.status_code == 503:
                raise ServiceUnavailableError(
                    f"The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, response: {token_response.text}"
                )
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.text}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        self.expires_in = token_json.get("expires_in", self._default_expiration) - 60
        if self.expires_in is None:
            self.logger.debug(
                "No expires_in receied in OAuth response and no "
                "default_expiration set. Token will be treated as if it never "
                "expires."
            )
        self.last_refreshed = request_time

        # store access_token in config file
        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["refresh_token"] = token_json["refresh_token"]

        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
